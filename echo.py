from collections import defaultdict
import logging
import socket
import select


class EchoServer(object):
    BUFSIZE = 4096  # Number of bytes requested in socket.recv

    def __init__(self, host='', port=50000):
        logging.info('Starting echo server')
        self.to_read = set()
        self.read_buffers = defaultdict(bytearray)
        self.write_buffers = defaultdict(bytearray)
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_socket.bind((host, port))
        self.server_socket.listen(1)
        self.to_read.add(self.server_socket)

    def _handle_read(self, sock, bufsize=BUFSIZE):
        """
        Read some data from the socket. If we hit a new line, add to the write buffer.
        """
        read_buffer = self.read_buffers[sock]
        write_buffer = self.write_buffers[sock]

        # Kill the socket if an error occurs or we receive 0 bytes
        try:
            val = sock.recv(bufsize)
        except socket.error:
            logging.exception('Unable to read from socket')
            return False, False

        if not val:
            return False, False

        while True:
            nl = val.find('\n')
            if nl == -1:
                break

            # Move preceeding line to the write buffer
            write_buffer.extend(read_buffer)
            del read_buffer[::]
            write_buffer.extend(val[:nl + 1])
            val = val[nl + 1:]

        # Store incomplete lines in the read buffer
        read_buffer.extend(val)

        return True, bool(write_buffer)

    def _handle_write(self, sock):
        """
        Try to write some data to the socket. If some bytes aren't written,
        leave it in the buffer.
        It would be simpler to use sendall() here but I guess this could block
        other clients.
        """
        write_buffer = self.write_buffers[sock]
        size = len(write_buffer)
        try:
            sent = sock.send(write_buffer)
        except socket.error:
            logging.exception('Unable to write to socket')
            return False, False

        del write_buffer[:sent]
        return sent != 0, bool(write_buffer)


    def _select(self, to_write):
        """
        Use select to get sockets ready for read/write
        """
        readable, writeable, _errored = select.select(self.to_read, to_write, [])

        # Each iteration, keep track of any sockets that errored, and any
        # we read newlines from.
        to_write = set()
        dead = set()

        for sock in readable:
            if sock is self.server_socket:
                # Accept new connections
                try:
                    client_socket, addr = sock.accept()
                    logging.info('Opened socket for %s. %d open sockets.', addr, len(self.to_read))

                    self.to_read.add(client_socket)
                except socket.error:
                    logging.exception('Unable to accept')
            else:
                # Read data
                write_buffer = self.write_buffers[sock]
                read_buffer = self.read_buffers[sock]
                success, ready_to_write = self._handle_read(sock)
                if not success:
                    dead.add(sock)
                elif ready_to_write:
                    to_write.add(sock)

        # Write echos
        for sock in writeable:
            if sock in dead:
                continue

            success, ready_to_write = self._handle_write(sock)
            if not success:
                dead.add(sock)
            elif ready_to_write:
                to_write.add(sock)

        return to_write, dead

    def select_loop(self):
        """
        Main loop
        """
        to_write = set()
        while True:
            to_write, dead = self._select(to_write)
            to_write = to_write - dead

            for sock in dead:
                if sock in self.to_read:
                    self.to_read.remove(sock)
                sock.close()

    def run(self):
        """
        Loop until ^C, then shutdown
        """
        try:
            self.select_loop()
        except KeyboardInterrupt:
            for sock in self.to_read:
                sock.shutdown(socket.SHUT_RDWR)
                sock.close()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    server = EchoServer()
    server.run()
