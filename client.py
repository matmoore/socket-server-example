import sys
import socket
import time
import itertools


def slow_client(host='localhost', port=50000, text='hello world\n'):
    print text
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host, port))
    while True:
        for char in itertools.cycle(text):
            ret = sock.send(char)
            if not ret:
                return
            if char == '\n':
                print sock.recv(20),
            time.sleep(0.1)


if __name__ == '__main__':
    slow_client(text=sys.argv[1] + '\n')
